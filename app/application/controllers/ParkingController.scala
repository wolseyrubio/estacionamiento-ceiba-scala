package application.controllers

import domain.models.{ParkingLot, Vehicle}
import javax.inject._
import domain.models.Vehicle
import infrastructure.persistence.{ParkingLotRepository, VehicleRepository}
import play.api.mvc._
import play.api.libs.json.Json

import scala.concurrent.{ExecutionContext, Future}

@Singleton
class ParkingController @Inject()(cc : ControllerComponents, vehicleRepository: VehicleRepository, parkingLotRepository: ParkingLotRepository)(implicit executionContext: ExecutionContext) extends AbstractController(cc){

  createParking()

  def index() = Action.async { implicit request: Request[AnyContent] =>

    val fVehicles: Future[Seq[Vehicle]] = vehicleRepository.all()

    fVehicles.map(s => Ok(Json.toJson(s)))
  }

  def getAllVehicles() = Action.async { implicit request: Request[AnyContent] =>

    val fVehicles: Future[Seq[Vehicle]] = vehicleRepository.all()

    fVehicles.map(s => Ok(Json.toJson(s)))
  }

  def addVehicle() = Action.async(parse.json[Vehicle]) { request =>
    insertVehicle(request.body)
  }

  private def insertParkingLot(parkingLot: ParkingLot): Future[Result] = {
    parkingLotRepository.insert(parkingLot)
      .map(_ => Ok(""))
      .recoverWith {
        case _: Exception => Future.successful(InternalServerError("No pudo guardarse el registro"))
      }
  }

  private def insertVehicle(vehicle: Vehicle): Future[Result] = {
    vehicleRepository.insert(vehicle)
      .map(_ => Ok("Inserted vehicle"))
      .recoverWith {
        case _: Exception => Future.successful(InternalServerError("No pudo guardarse el registro"))
      }
  }

  private def createParking() {
    insertParkingLot(new ParkingLot(1, 1 , 40))
    insertParkingLot(new ParkingLot(2, 2 , 50))
  }

}
