package infrastructure.persistence

import domain.models.ParkingLot
import javax.inject.{Inject, Singleton}
import play.api.db.slick.{DatabaseConfigProvider, HasDatabaseConfigProvider}
import slick.jdbc.JdbcProfile

import scala.concurrent.{ExecutionContext, Future}

@Singleton
class ParkingLotRepository @Inject()(protected val dbConfigProvider: DatabaseConfigProvider)(implicit executionContext: ExecutionContext) extends HasDatabaseConfigProvider[JdbcProfile]{
  import profile.api._

  private val ParkingsLot = TableQuery[ParkingLotTable]

  def all() : Future[Seq[ParkingLot]] = db.run(ParkingsLot.result)
  def insert(parkingLot: ParkingLot): Future[Unit] = db.run(ParkingsLot += parkingLot).map { _ => () }
  // def lower(limit: Int): Future[Seq[Vehicle]] = db.run(Vehicles.filter(_.price < limit.toDouble).result)

  private class ParkingLotTable(tag: Tag) extends Table[ParkingLot](tag, "PARKING_LOT") {

    def idParkingLot = column[Int]("ID_PARKING_LOT", O.PrimaryKey, O.AutoInc)
    def vehicleType = column[Int]("VEHICLE_TYPE")
    def numberOfVehicle = column[Int]("NUMBER_OF_VEHICLE")


    def * = (idParkingLot, vehicleType, numberOfVehicle) <> ( (ParkingLot.apply _).tupled, ParkingLot.unapply)
  }


}
