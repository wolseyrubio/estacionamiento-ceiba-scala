package infrastructure.persistence

import java.sql.Date

import domain.models.Vehicle
import javax.inject.{Inject, Singleton}
import play.api.db.slick.{DatabaseConfigProvider, HasDatabaseConfigProvider}
import slick.jdbc.JdbcProfile

import scala.concurrent.{ExecutionContext, Future}

@Singleton
class VehicleRepository @Inject()(protected val dbConfigProvider: DatabaseConfigProvider) (implicit executionContext: ExecutionContext) extends HasDatabaseConfigProvider[JdbcProfile]{
  import profile.api._

  private val Vehicles = TableQuery[VehicleTable]

  def all() : Future[Seq[Vehicle]] = db.run(Vehicles.result)
  def insert(vehicle: Vehicle): Future[Unit] = db.run(Vehicles += vehicle).map { _ => () }
 // def lower(limit: Int): Future[Seq[Vehicle]] = db.run(Vehicles.filter(_.price < limit.toDouble).result)

  private class VehicleTable(tag: Tag) extends Table[Vehicle](tag, "VEHICLE") {

    def idVehicle = column[Int]("ID_VEHICLE", O.PrimaryKey)
    def licensePlateVehicle = column[String]("LICENSE_PLATE_VEHICLE")
    def dateOfAdmission = column[Date]("DATE_OF_ADMISSION")
    def departureDate = column[Date]("DEPARTURE_DATE")
    def vehicleCylinder = column[Long]("VEHICLE_CYLINDER")
    def idParkingLot = column[Int]("ID_PARKING_LOT")
    def vehicleType = column[String]("VEHICLE_TYPE")
    def state = column[Boolean]("STATE")
    def price = column[Double]("PRICE")

    def * = (idVehicle, licensePlateVehicle, dateOfAdmission, departureDate, vehicleCylinder, idParkingLot, vehicleType, state, price) <> ( (Vehicle.apply _).tupled, Vehicle.unapply)
  }


}
