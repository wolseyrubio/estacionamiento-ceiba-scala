package domain.models

import java.sql.Date

import play.api.libs.json.Json

case class Vehicle(idVehicle: Int, licensePlateVehicle: String, dateOfAdmission: Date, departureDate: Date, vehicleCylinder : Long, idParkingLot: Int, vehicleType: String, state: Boolean, price: Double)

object Vehicle {
  implicit val vehicleFormat = Json.format[Vehicle]
}
