package domain.models

import play.api.libs.json.Json

case class ParkingLot(idParkingLot: Int, vehicleType: Int, numberOfVehicle: Int)

object ParkingLot {
  implicit val parkingLotFormat = Json.format[ParkingLot]
}
