package controllers

import application.controllers.ParkingController
import javax.inject.Inject

import scala.concurrent.{ExecutionContext, Future}
import org.scalatestplus.play._
import infrastructure.persistence.{ParkingLotRepository, VehicleRepository}
import play.api.mvc._
import play.api.test._
import play.api.test.Helpers._

class ParkingControllerTest  @Inject()(cc : ControllerComponents,vehicleRepository: VehicleRepository, parkingLotRepository: ParkingLotRepository)(implicit executionContext: ExecutionContext) extends PlaySpec with Results {

  "testGetAllVehicles" should  {
    "should be valid" in {
      val controller = new ParkingController(cc, vehicleRepository, parkingLotRepository)

      val result: Future[Result] = controller.index().apply(FakeRequest())
      val bodyText: String = contentAsString(result)
      bodyText mustBe "ok"
    }
  }

  "testIndex" should {

  }

  "testAddVehicle" should  {

  }

}
